# LuaOxygen

Aims to extend Lua stdlib with intuitive ways of dealing with
tables and most common tasks.

Examples:

* o2.cli
	* Read stdin
	* Parse cli arguments easily (not as getopts)

* o2.dict
	* Create dict tables (metatable with operations)
	* Handle common tables as dictionaries providind most
	  used actions like type checking, key checking etc.

* o2.list
	* Create list tables (metatable with operations)
	* Functional approach of filter, sort etc.

* ... as with needed

This project started as a frustration with Python. Python is
simple but fails in its standards "one way of do things",
especially in case of eggs, pycache, comprehension ugliness
and more heavy footprint with possible optimizations with
refactoring code.

As Python has a more complete ecosystem in its stdlib, this
project aims to get the positive things that makes possible
code without rewrite basic things like loops or have a mind
strain on processes like data manipulation.

For complete api, read [doc/manual.md]

If you want to contribute to the code keep in mind that:

* The function you wrote is really about a common used task?
* The function really makes the task simple?
* This has little to none side effects?
* If someone think about the task, this function is easily
  spotted?


