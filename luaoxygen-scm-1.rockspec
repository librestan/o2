package = "luaoxygen"
version = "scm-1"
source = {
   url = "git+https://bitbucket.org/librestan/o2.git"
}
description = {
   summary = "Lua stdlib expansion target on simplicity",
   detailed = [[
      Common tasks in development grouped into functions
   ]],
   license = "MIT/X11"
}
dependencies = {
   "lua >= 5.1"
}
build = {
   type = "builtin",
   modules = {
      o2 = "init.lua"
   },
   copy_directories = {
      "doc",
   }
}

